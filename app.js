const express = require('express');
const createError = require('http-errors');
const dotenv = require('dotenv').config();
const cookieParser = require("cookie-parser");
const userService = require('./Services/UserService')
const StatusError = require('./Errors/StatusError')

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

// Initialize DB
require('./initDB')();
app.use((req, res, next) => {
  setDefaultHeaders(res);
  next();
})

app.use((req, res, next) => {
  if(req.method !== 'GET' && req.method !== 'OPTIONS' && !req.path.toLowerCase().includes("/users")){
    console.log(req.cookies);
    authenticateAcessToken(req);
  }
  next();
});

const CyberAttacksRoute = require('./Routes/CyberAttacks.route');
app.use('/cyberAttacks', CyberAttacksRoute);

const CyberProtectionRoute = require('./Routes/CyberProtection.route');
app.use('/cyberProtections', CyberProtectionRoute);

const TrainingMaterialRoute = require('./Routes/TrainingMaterial.route');
app.use('/trainingMaterials', TrainingMaterialRoute);

const ContactPersonRoute = require('./Routes/ContactPerson.route');
app.use('/contactPersons', ContactPersonRoute);

const UsersRoute = require('./Routes/Users.route');
app.use('/users', UsersRoute)

//404 handler and pass to error handler
app.use((req, res, next) => {
  next(createError(404, 'Site not found'));
});

//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message
    }
  });
});
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log('Server started on port ' + PORT + '...');
});

const setDefaultHeaders = (res) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:5000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader("Content-Type", "application/json");
}

const authenticateAcessToken = (req) => {
  const authenticationCookie = req.cookies['Authorization']
  if(authenticationCookie === undefined) {
    throw new StatusError(401, "Kein korrekter Authentication Cookie angegeben")
  }
  const authToken = authenticationCookie.split(' ')[1]
  if(authToken==null){
    throw new StatusError(401, "Kein korrekter Authentication Header angegeben")
  }
  userService.authenticateAccessToken(authToken);
}

