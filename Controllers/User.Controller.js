const UserService = require('../Services/UserService')

module.exports ={
    registerNewUser: async (req, res, next) => {
        try{
            body = req.body;
            userinfo = {
                username:req.body.username,
                mail:req.body.mail,
                password:req.body.password,
            }
            await UserService.registerNewUser(userinfo);
            res.status(200).send();
        }catch (error) {
            console.log(error.message);
            next(error);
          }
    },

    login: async(req, res, next) => {
        try{
            body = req.body;
            loginInfo = {
                mail:req.body.mail,
                password:req.body.password,
            }
            token = await UserService.authenticate(loginInfo);
            res.cookie('Authorization', 'Bearer '+token, {sameSite:'Strict', httpOnly: true, secure: false});
            res.status(200).send();
        }catch(error) {
            console.log(error.message);
            next(error);
        }
    },

    logout: async(req, res, next) => {
        try{
            res.clearCookie('Authorization');
            res.status(205).end();
        }catch(error) {
            console.log(error.message);
            next(error);
        }
    }
}