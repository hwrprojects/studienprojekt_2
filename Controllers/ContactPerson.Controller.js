const createError = require('http-errors');
const mongoose = require('mongoose');

const ContactPerson = require('../Models/ContactPerson.model');

module.exports = {
  getAllContactPerson: async (req, res, next) => {
    try {
      const results = await ContactPerson.find();
      res.status(200).send(results);
    } catch (error) {
      console.log(error.message);
      next(error);
    }
  },

  createNewContactPerson: async (req, res, next) => {
    try {
      const contactPerson = new ContactPerson(req.body);
      const result = await contactPerson.save();
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error.name === 'ValidationError') {
        next(createError(422, error.message));
        return;
      }
      next(error);
    }
  },

  findContactPersonById: async (req, res, next) => {
    const id = req.params.id;
    try {
       const contactPerson = await ContactPerson.findOne({ _id: id });
      if (!contactPerson) {
        throw createError(404, 'ContactPerson does not exist.');
      }
      res.status(200).send(contactPerson);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid ContactPerson id'));
        return;
      }
      next(error);
    }
  },

  updateAContactPerson: async (req, res, next) => {
    try {
      const id = req.params.id;
      const updates = req.body;
      const options = { new: true };

      const result = await ContactPerson.findByIdAndUpdate(id, updates, options);
      if (!result) {
        throw createError(404, 'ContactPerson does not exist');
      }
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        return next(createError(400, 'Invalid ContactPerson Id'));
      }

      next(error);
    }
  },

  deleteAContactPerson: async (req, res, next) => {
    const id = req.params.id;
    try {
      const result = await ContactPerson.findByIdAndDelete(id);
      if (!result) {
        throw createError(404, 'ContactPerson does not exist.');
      }
      const response = {
        message: "Todo successfully deleted",
        id: req.params.id
      };
      return res.status(200).send(response);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid ContactPerson id'));
        return;
      }
      next(error);
    }
  }
};
