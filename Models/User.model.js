const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    ID: Number,
    username: String,
    mail: String,
    passwordHash: String,
    salt: String,
}, { versionKey: false });



const User = mongoose.model('user', UserSchema,'user');
module.exports = User;
