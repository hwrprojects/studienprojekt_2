const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CyberProtectionSchema = new Schema({
  ID: Number,
  name: String,
  description: String,
  difficulty: Number,
  contactPerson: mongoose.Types.ObjectId
}, { versionKey: false });

const CyberProtection = mongoose.model('cyberProtection', CyberProtectionSchema,'cyberProtection');
module.exports = CyberProtection;
