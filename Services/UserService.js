const User = require("../Models/User.model")
const PasswordEncrypterService = require("./PasswordEncrypterService")
const jwt = require('jsonwebtoken')
const StatusError = require('../Errors/StatusError')

findUserByUsername = async (username) =>  {
   const user = await User.findOne({username: username}).exec();
   return user;
}

findUserByEmail = async (email) =>  {
   const user = await User.findOne({mail: email}).exec();
   return user;
}

validateUsernameNotAlreadyTaken = async (username) => {
   user = await findUserByUsername(username)
   if(user === undefined || user === null) {
      return;
   }
   console.log(user);
   throw new StatusError(400, "Username already taken")
}

pushUserInfoToUser = (userInfo) => {
   user = new User();
   user.username = userInfo.username;
   user.mail = userInfo.mail;

   return user;
}

exports.registerNewUser = async (userInfo) => {
   console.log(userInfo);
   await validateUsernameNotAlreadyTaken(userInfo.username);
   encodedPassword = await PasswordEncrypterService.encodePassword(userInfo.password);

   user = pushUserInfoToUser(userInfo);
   user.passwordHash = encodedPassword.encodedPassword;
   user.salt = encodedPassword.salt;

   console.log("Saving User")
   user.save();
}

exports.authenticate = async (loginInfo) => {
   console.log("Authenticating")
   console.log(loginInfo)
   user = await findUserByEmail(loginInfo.mail);
   console.log(user);
   if(user === undefined){
      throw new StatusError(401, "Credentials do not match")
   }
   try{
      await PasswordEncrypterService.validatePassword(user, loginInfo.password)
   }catch(err){
      throw new StatusError(401, "Credentials do not match")
   }
   return jwt.sign(loginInfo, process.env.SESSION_SECRET);
}

exports.authenticateAccessToken = (token) => {
   try{
      jwt.verify(token, process.env.SESSION_SECRET);
   }catch(err){
      console.log(err)
      throw new StatusError(403, "Invalid Token")
   }
}



