var bcrypt = require('bcryptjs');

exports.encodePassword = async (password) => {
    encodedPassword = await encodePassword(password);
    return{
        encodedPassword: encodedPassword
    }
},

exports.validatePassword = async (user, password) => {
    areEqual = await bcrypt.compare(password, user.passwordHash);
    if(areEqual == true) {
        return true;
    }
    throw new Error('Credentials do not match')
},

encodePassword = async (password) => {
    console.log("encoding Pasword...")
    const salt = await bcrypt.genSalt(12)
    const hash = await bcrypt.hash(password, salt); 
    return hash
}