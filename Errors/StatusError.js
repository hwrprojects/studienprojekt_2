 class StatusError extends Error {
    constructor(statuscode, message) {
      super(message); 
      this.name = "StatusError";
      this.status = statuscode;
    }
  }
  module.exports = StatusError