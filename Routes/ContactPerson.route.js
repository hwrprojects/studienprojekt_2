
const express = require('express');
const router = express.Router();

const ContactPersonController = require('../Controllers/ContactPerson.Controller');

//Get a list of all ContactPerson
router.get('/', ContactPersonController.getAllContactPerson);

//Create a new ContactPerson
router.post('/', ContactPersonController.createNewContactPerson);

//Get a ContactPerson by id
router.get('/:id', ContactPersonController.findContactPersonById);

//Update a ContactPerson by id
router.patch('/:id', ContactPersonController.updateAContactPerson);

//Delete a ContactPerson by id
router.delete('/:id', ContactPersonController.deleteAContactPerson);

module.exports = router;
