const express = require('express');
const router = express.Router();

const UserController = require('../Controllers/User.Controller');

//Register a new User
router.post('/', UserController.registerNewUser);

//Login a new User
router.post('/login', UserController.login)

//Login a new User
router.delete('/logout', UserController.logout);

module.exports = router;